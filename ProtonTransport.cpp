#include "ProtonTransport.h"
#include <iostream>
#include <fstream>

#include "AtlasLabels.C"
#include "AtlasLabels.h"
#include "AtlasUtils.C"
#include "AtlasUtils.h"

#include "TROOT.h"    
#include "TObject.h" 
#include "TFile.h"
#include "TTree.h"
#include "TH2.h"
#include "TF1.h"
#include "TMath.h"
#include "TGraph.h"
#include "TGraph2D.h"
#include "TGraphErrors.h"
#include "TColor.h"
#include "TCanvas.h"
#include "TLegend.h"
#include <TStyle.h>
#include "TLatex.h"
#include "TProfile.h"

using namespace std;

int main(){

  string whichDetector; 
  string whichStation;
  double BD_MM_MIN;
  double BD_MM_MAX;
  double safety_dist;
  //-------------------------------------------------------------------------------
  for(int AorA=1; AorA<=2; AorA++){
    safety_dist = 0.8*1e-3; //in meter
    std::cout << "Safety distance: " << safety_dist << std::endl;
    if (AorA==1) 
      {
        whichDetector = "AFP";
        BD_MM_MIN = 11.5;
        BD_MM_MAX = 11.5;
      }
    else if (AorA==2)
      {
       whichDetector = "ALFA";
       BD_MM_MIN = 11.5;
       BD_MM_MAX = 11.5;
      }
  for(int NorF=1; NorF<=2; NorF++){
    if (NorF==1) whichStation = "NS";
    else if (NorF==2) whichStation = "FS";
  for(int collimator_opening=0; collimator_opening<=100; collimator_opening+=20){

  for(double BD_MM=BD_MM_MIN; BD_MM<=BD_MM_MAX; BD_MM+=1.5){
  //-------------------------------------------------------------------------------      

    cout << whichDetector << "  " << whichStation << "  C: " << collimator_opening << endl;

  const char*atlaslabel = " Work in progress";
  //const char*atlaslabel = " Simulation";
  bool save_beamtrajectory = true; //Only needed once when TWISS file is changed.
  bool save_acceptance     = true; //Needed once when the ranges of the loops are changed.
  //string whichDetector = "ALFA"; // "AFP" for AFP detector results and "ALFA" for ALFA detector results.
  //string whichStation = "FS"; //"NS" for near station and "FS" for far station. By default will take near station.
  //int collimator_opening = 100;
  double beam_energy = 6500.;
  string CM = "13";// Center of mass energy for the titles in plots.

  int TwissFile = 3; //1 for "alfaTwiss1.txt_beta30cm_6500GeV_y140murad". 
                     //2 for "alfaTwiss1.txt_Run2_beta19m_6500GeV_y-145murad".
                     //3 for "alfaTwiss1.txt_Run3_2022_13_6TeV_beta19p2m_thetaC170murad_phi90".
  if(TwissFile == 1 or TwissFile == 2) beam_energy = 6500.;
  else if(TwissFile == 3) beam_energy = 6800.;
  float loopPar[3] = {100.,100.,0.1}; //Loop step amounts in the form {E=10,phi=100,pT=0.1}. 
  //string fileName = ("Twiss"+to_string(TwissFile)+"E1phi60Pt01_"+whichStation).c_str();//File name to create data files and plots.
  string fileName = (to_string(BD_MM)+"sigma_C"+to_string(collimator_opening)+"_"+whichDetector+"_"+whichStation+"_"+to_string((int)round(beam_energy))+"GeV").c_str();
  //string fileName = ("C"+to_string(collimator_opening)+"_"+whichDetector+"_"+whichStation+"_"+to_string((int)round(beam_energy))+"GeV").c_str();


  ProtonTransport * p;
  p = new ProtonTransport;

  //beam_energy = 6500.;
  //energy must be set correspondingly to one from twiss file
  p->SetBeamEnergy(beam_energy);
  cout << "Beam energy:\t" << p->GetBeamEnergy() << " [GeV]" << endl;
  p->SetEmittance(3.5);
  cout << "Emittance:\t" << p->GetEmittance() << " [mumrad]" << endl;
  p->SetDoApertureCut(true);
  cout << "DoApertureCut:\t" << p->GetDoApertureCut() << " " << endl;

  p->SetTCL4Sigma(collimator_opening);
  cout << "TCL4Sigma:\t" << p->GetTCL4Sigma() << " " << endl;
  p->SetTCL5Sigma(2*collimator_opening);
  cout << "TCL5Sigma:\t" << p->GetTCL5Sigma() << " " << endl;
  p->SetTCL6Sigma(collimator_opening);
  cout << "TCL6Sigma:\t" << p->GetTCL6Sigma() << " " << endl;


  string betaStar, crossingAngle;
  if (TwissFile == 1)
  {
    betaStar = "30 c";
    crossingAngle = "280";
    beam_energy = 6500.;
    CM = "13";// Center of mass energy for the titles in plots.
    //(half) crossing angle must be set correspondingly to one from twiss file
    p->SetCrossingAngle(140.e-6);
    cout << "Crossing angle:\t" << p->GetCrossingAngle()*1.e6 << " [murad]" << endl;
    //the following columns are expected to be in twiss file: Mandatory: KEYWORD, S, L, HKICK, VKICK, K0L, K1L, K2L, K3L
    //If one of these is not provided, the aperture will NOT be considered: APERTYPE, APER_1, APER_2, APER_3, APER_4
    //If one of these is not provided, the beam width will not be computed: BETX, BETY
    //These values are optional. For now they are not used in the tracking code. Still, if they are missing a warning will appear: X, Y, PX, PY
    p->PrepareBeamline("alfaTwiss1.txt_beta30cm_6500GeV_y140murad", false);
    //p->PrepareBeamline("alfaTwiss1.txt_Run2_beta19m_6500GeV_y-145murad", false);
  }
  else if (TwissFile == 2)
  {
    betaStar = "19.2 ";
    crossingAngle = "-290";
    beam_energy = 6500.;
    CM = "13";// Center of mass energy for the titles in plots.
    //(half) crossing angle must be set correspondingly to one from twiss file
    p->SetCrossingAngle(-145.e-6);
    cout << "Crossing angle:\t" << p->GetCrossingAngle()*1.e6 << " [murad]" << endl;
    //the following columns are expected to be in twiss file: Mandatory: KEYWORD, S, L, HKICK, VKICK, K0L, K1L, K2L, K3L
    //If one of these is not provided, the aperture will NOT be considered: APERTYPE, APER_1, APER_2, APER_3, APER_4
    //If one of these is not provided, the beam width will not be computed: BETX, BETY
    //These values are optional. For now they are not used in the tracking code. Still, if they are missing a warning will appear: X, Y, PX, PY
    //p->PrepareBeamline("alfaTwiss1.txt_beta30cm_6500GeV_y140murad", false);
    p->PrepareBeamline("alfaTwiss1.txt_Run2_beta19m_6500GeV_y-145murad", false);
  }
  else if (TwissFile == 3)
  {
    betaStar = "19.2 ";
    crossingAngle = "-290";
    beam_energy = 6800.;
    CM = "13.6";// Center of mass energy for the titles in plots.
    //(half) crossing angle must be set correspondingly to one from twiss file
    p->SetCrossingAngle(145.e-6);
    p->SetCrossingAnglePhi(270);
    cout << "Crossing angle:\t" << p->GetCrossingAngle()*1.e6 << " [murad]" << endl;
    //the following columns are expected to be in twiss file: Mandatory: KEYWORD, S, L, HKICK, VKICK, K0L, K1L, K2L, K3L
    //If one of these is not provided, the aperture will NOT be considered: APERTYPE, APER_1, APER_2, APER_3, APER_4
    //If one of these is not provided, the beam width will not be computed: BETX, BETY
    //These values are optional. For now they are not used in the tracking code. Still, if they are missing a warning will appear: X, Y, PX, PY
    //p->PrepareBeamline("alfaTwiss1.txt_beta30cm_6500GeV_y140murad", false);
    p->PrepareBeamline("alfaTwiss1.txt_Run3_2022_13p6TeV_beta19p2m_thetaC145murad_phi270", false);
  }

  //an example how to retrive information about colliamtors (computed in PrepareBeamline)
  //cout << "TCL4\tz: " << p->GetTCL4Pos() << " [m]\tcenter: " << p->GetTCL4Center()*1.e3 << " [mm]\t jaw: " << p->GetTCL4Jaw()*1.e3 << " [mm] (around center)" << endl;
  //cout << "TCL5\tz: " << p->GetTCL5Pos() << " [m]\tcenter: " << p->GetTCL5Center()*1.e3 << " [mm]\t jaw: " << p->GetTCL5Jaw()*1.e3 << " [mm] (around center)" << endl;
  //cout << "TCL6\tz: " << p->GetTCL6Pos() << " [m]\tcenter: " << p->GetTCL6Center()*1.e3 << " [mm]\t jaw: " << p->GetTCL6Jaw()*1.e3 << " [mm] (around center)" << endl;

  //uncomment if info about nominal trajectory is needed
  //p->NominalTrajectory();
 
  //cout << endl;
  
  double observed_point;
  if (whichDetector.compare("AFP")== 0)
  {
    if (whichStation.compare("NS")==0) observed_point = 205.217;//NEAR station
    else if (whichStation.compare("FS")==0) observed_point = 217.302;//FAR station
  } 
  else if (whichDetector.compare("ALFA")==0)
  {
    if (whichStation.compare("NS")==0) observed_point = 237.;//NEAR station
    else if (whichStation.compare("FS")==0) observed_point = 245.;//FAR station
  }
  else
  {
    observed_point = 500.; //To get all the data points
  }

  
  cout << "Observed point: "<< observed_point << " m" << endl;

  //-----------------------------------------------------------------------------------------
  std::vector<std::vector <double>> accV;
  std::vector<double> temp;
  std::vector<std::vector <double>> ellipses;
  std::vector<double> ellipse;
  //-----------------------------------------------------------------------------------------
  /*
  for (int i=0; i<beam_width.size(); i++) 
  { 

    cout << "Beam width[mm]:\t" << beam_width[i] << endl;
    cout << "Beam-Detector Distance[mm]:\t" << beam_width[i]*8 << " +- " << beam_width[i]*3+0.8 << endl;
  }
  */
  //-----------------------------------------------------------------------------------------

  ofstream save;
  if (save_acceptance)
  {
    std::cout << "Raw acceptance data is saving!" << std::endl;
    save.open(("./data/acceptance_raw_"+fileName+".dat").c_str());
  }
  if (save_beamtrajectory || save_acceptance)
  {
	  //for (double e=5500.; e<=1.00001*6500.; e += 500.)
	  for (double e=5500.; e<=1.00001*beam_energy; e += loopPar[0])
	  {
	    //for (double phi = 0.; phi < 2.*3.1415; phi += 2.*3.1415/15.)
	    for (double phi = 0.; phi <= 2.*3.1415; phi += 2.*3.1415/loopPar[1])
	    {
	      //for (double pT = 0.; pT < 3.; pT += 3./5.)
	      for (double pT = 0.; pT <= 3.; pT += loopPar[2])
	      {
          //cout << e << "\t" << phi << "\t" << pT << endl;
	        //set proton properties
	        //note that information about crossing angle (hardcoded to be in x onlt TODO) will be added automatically
	        p->SetX(0.);
	        p->SetY(0.);
	        p->SetZ(0.);
	        p->SetPx(pT*std::sin(phi));
	        p->SetPy(pT*std::cos(phi));
	        p->SetPz(e);
	        p->SetSx(p->GetPx()/p->GetPz());
	        p->SetSy(p->GetPy()/p->GetPz());
	        
          //if (std::fabs(phi - 6.283) < 1E-3) cout << "HERE!!!!!!!!!!" << endl;
	        bool each_step=false; //default
	        if (pT==0 && e==beam_energy && phi==0) each_step = true;
	        
	        p->SimpleTracking(observed_point, p->GetX(), p->GetY(), p->GetZ(), p->GetPx(), p->GetPy(), p->GetPz(), each_step, false);

	        //position at observed_point
	        double x = p->GetX();
	        double y = p->GetY();
	        double z = p->GetZ();

	        //momentum at observed_point
	        double px = p->GetPx();
	        double py = p->GetPy();
	        double pz = p->GetPz();
	        
	        //proton is always tracked to observed_point, despite being (possibly) lost in aperture or collimators
	        //however, the first postion at which proton was lost is stored in lost_z (by default set to -1)
	        //hence in analysis a following condition should be used for "survived" protons: lost_z < 0 || lost_z > observed_point
	        double lost_z = p->GetLostPos();

	        //cout << "E: " << e << "\tpT: " << pT << "\tphi: " << phi << "\tx: " << x*1000 << "\ty: " << y*1000 << "\tz: " << z << "\tpx: " << px << "\tpy: " << py  << "\tpz: " << pz  << "\tlost_z: " << lost_z << endl;
			
			//-------------------------------------------------------
	        if (save_acceptance)
	        {
			  	double xi = (beam_energy-e)/(beam_energy);
			  	save << pT << " " << xi << " " << lost_z << " " << x << " " << y << endl;
			  	//std::cout << pT << "\t" << xi << "\t" << lost_z << std::endl;
	        }
	      }
	    }
	  }

	  if (save_acceptance) 
    {
      save << p->GetBeamCenter()[0] << " " << p->GetBeamCenter()[1] << " " << p->GetBeamCenter()[2] << " " << p->GetBeamCenter()[3] << " " << p->GetBeamCenter()[4] << endl;
      save.close();
    }
  }
  //-----------------------------------------------------------------------------------------------------------
  //-----------------------------------------------------------------------------------------------------------
  //-----------------------------------------------------------------------------------------------------------
  if (save_beamtrajectory) 
  {
    std::cout << "Beam trajectory data is saving!" << std::endl;
    std::vector<std::vector<double>> beam_trajectory = p->GetBeamTrajectory();
    ofstream save1;
    save1.open(("./data/beamtrajectory_"+fileName+".dat").c_str());
    cout << beam_trajectory.size() << endl;
    for (int i=0; i<beam_trajectory.size(); i++)
    {
      //
      if (beam_trajectory[i][2] >=0 && beam_trajectory[i][2] < 250) save1 << beam_trajectory[i][0] << " " << beam_trajectory[i][1] << " " << beam_trajectory[i][2] << endl;
        //std::cout << beam_trajectory[i][0] << "\t" << beam_trajectory[i][1] << "\t" << beam_trajectory[i][2] << std::endl;
        //if (beam_trajectory[i][2] > observed_point) cout << beam_trajectory[i][0] << "   !!!!!   " << beam_trajectory[i][1] << endl;
    } 
    save1.close();
  }
  //-----------------------------------------------------------------------------------------------------------
  if (save_acceptance) 
  {
  	cout << "Acceptance data is saving!" << endl;
	  //----------------------------------------------------------------------
	  ifstream inputFile;
	  inputFile.open(("./data/acceptance_raw_"+fileName+".dat").c_str());
	  while(!inputFile.eof())
		{
    	double pT, xi, lost_z, x, y;
    	inputFile >> pT >> xi >> lost_z >> x >> y;
    	temp.push_back(pT);
	    temp.push_back(xi);
	    temp.push_back(lost_z);
      temp.push_back(x);
      temp.push_back(y);
	    accV.push_back(temp);
	    temp.clear();
		}
	  
    std::vector<double> beam_center = accV.back();
    accV.pop_back();
    //cout << "#sigma_{x}: " << sqrt((beam_center[0]*3.5e-6)/(6.5e12/938e6))*1000. << "   #sigma_{y}: " << sqrt((beam_center[1]*3.5e-6)/(6.5e12/938e6))*1000. << "   Beam-Det Dis. x: " << BD_MM*sqrt((beam_center[0]*3.5e-6)/(6.5e12/938e6))*1000. + safety_dist*1000. << "   Beam-Det Dis. y: " << BD_MM*sqrt((beam_center[1]*3.5e-6)/(6.5e12/938e6))*1000. + safety_dist*1000.  << "   Beam pos. x: " << beam_center[3] << "   Beam pos. y: " << beam_center[4] << endl; 
    
    std::vector<std::vector<double>> beam_width = p->GetBeamWidth();
    int tempIndex;
    for (int i=0;i<beam_width.size();i++) {
      //cout << observed_point << " " << beam_width[i][2] << endl;
      if(abs(observed_point-beam_width[i][2])<1) {
        cout << "#sigma_{x}: " << beam_width[i][0] << "   #sigma_{y}: " << beam_width[i][1] << "   Beam-Det Dis. x: " << BD_MM*beam_width[i][0] + safety_dist*1000. << "   Beam-Det Dis. y: " <<  BD_MM*beam_width[i][1] + safety_dist*1000.  << "   Beam pos. x: " << beam_center[3] << "   Beam pos. y: " << beam_center[4] << endl; 
        cout << "   Beam-Det Dis. x: " << BD_MM*beam_width[i][0] + safety_dist*1000. << "   Beam-Det Dis. y: " << BD_MM*beam_width[i][1] + safety_dist*1000. << endl; 
        tempIndex = i;
      }
    }
    cout << sqrt((beam_center[0]*3.5e-6)/(6.5e12/938e6)) << " |beam widths| " << beam_width[tempIndex][0]/1000 << endl;
    std::vector<double> AFP_x_limit {beam_center[3] - BD_MM*(beam_width[tempIndex][0]/1000) - safety_dist, beam_center[3] - BD_MM*beam_width[tempIndex][0] - safety_dist - 0.0162};
    //std::vector<double> AFP_x_limit {beam_center[3] - BD_MM*sqrt((beam_center[0]*3.5e-6)/(6.5e12/938e6)) - safety_dist, beam_center[3] - BD_MM*sqrt((beam_center[0]*3.5e-6)/(6.5e12/938e6)) - safety_dist - 0.0162};
    std::vector<double> AFP_y_limit { -0.01, 0.01};
    std::vector<double> ALFA_x_consts {0.01365, 0.022275};
    std::vector<double> ALFA_y_consts {BD_MM*(beam_width[tempIndex][1]/1000)+safety_dist, 0.008625};
    //std::vector<double> ALFA_y_consts {(BD_MM*sqrt((beam_center[1]*3.5e-6)/(6.5e12/938e6)))+safety_dist, 0.008625};
    std::vector<double> ALFA_y_limit {beam_center[4] + ALFA_y_consts[0], beam_center[4] + ALFA_y_consts[0] + ALFA_y_consts[1], beam_center[4] + ALFA_y_consts[0] + 0.0309};
    cout << "Beam x position: " << beam_center[3] << "  |  Beam y position: " << beam_center[4] << endl;
    //cout << "ALFA y[0]: " << ALFA_y_consts[0] << "  |  ALFA y[1]: " << ALFA_y_consts[1] << endl;

	  ofstream save2;
    save2.open(("./data/acceptance_"+fileName+".dat").c_str());
    cout <<  ("./data/acceptance_"+fileName+".dat").c_str() << endl;
    //for (int i=0; i<1000; i++) cout << accV[i][0] << "\t" << accV[i][1] << "\t" << accV[i][2] << endl;
    cout << "Sorting..." << endl;
    std::sort(accV.begin(), accV.end());
    //for (int i=0; i<100000; i++) cout << accV[i][0] << "\t" << accV[i][1] << "\t" << accV[i][2] << endl;

    int p_reach = 0;
	  int p_total = 1;
    int passed = 0;
	  for (int i=0; i<accV.size(); i++)
	  {
      passed = 0;
	    //cout << accV[i][3] << " :x::y: " << accV[i][4] << endl;
      //cout << AFP_y_limit[1] << " :1::2: " << AFP_y_limit[2] << endl;
      //if (abs(accV[i][4]) > ALFA_y_limit[1]) cout << abs(accV[i][4]) << "::" << ALFA_y_limit[1]  << endl;
	    if (i==0) continue;
	    if (accV[i-1][0] == accV[i][0] and accV[i-1][1] == accV[i][1])
	    {
	    	if (accV[i][2] < 0 or accV[i][2] >= observed_point)
	      {
          if (whichDetector.compare("AFP")==0 && accV[i][3] < AFP_x_limit[0] && accV[i][3] > AFP_x_limit[1] && accV[i][4] > AFP_y_limit[0] && accV[i][4] < AFP_y_limit[1])
          {
            p_reach++;
            passed = 1;
          }
          //else if (whichDetector.compare("ALFA")==0 && abs(accV[i][3]) < ALFA_x_limit[1] && abs(accV[i][4]) > ALFA_y_limit[0] && abs(accV[i][4]) < AFP_y_limit[1])
          else if (whichDetector.compare("ALFA")==0)
          {
            if (abs(accV[i][4]) > ALFA_y_limit[0] && abs(accV[i][4]) < ALFA_y_limit[1])
            {
              //if (abs(accV[i][3]) < ((ALFA_x_consts[0]*(ALFA_y_consts[0]+ALFA_y_consts[1]-abs(accV[i][4])))+(ALFA_x_consts[1]*(abs(accV[i][4])-ALFA_y_consts[0])))/(ALFA_y_consts[1]) )
              if (abs(accV[i][3]) <  ALFA_x_consts[1] - (((ALFA_y_consts[1]-(abs(accV[i][4])- ALFA_y_consts[0]))*(ALFA_x_consts[1]- ALFA_x_consts[0]))/ALFA_y_consts[1]) )
              {
                p_reach++; 
                passed = 1;
              } 
            }
            else if (abs(accV[i][4]) >= ALFA_y_limit[1] && abs(accV[i][4]) < ALFA_y_limit[2])
            {
              if (abs(accV[i][3]) < (0.0309 + ALFA_y_consts[0] - abs(accV[i][4]))) 
              {
                p_reach++;
                passed = 1;
              }  
            }
          }
	      }
	      p_total++;
	    }
	    else
	    {
	    	
	    	//accV[i][2] = ((double)p_reach/(double)p_total)*100;
	    	//cout << "reach - total: " << p_reach << "\t" << p_total << "\t" << accV[i][2] << endl;
	    	save2 << accV[i-1][0] << " " << accV[i-1][1] << " " << ((double)p_reach/(double)p_total)*100 << endl;
        p_reach = 0;
        p_total = 1;
	    	if (accV[i][2] < 0 or accV[i][2] > observed_point)
        {
          if (whichDetector.compare("AFP")==0 && accV[i][3] < AFP_x_limit[0] && accV[i][3] > AFP_x_limit[1] && accV[i][4] >  AFP_y_limit[0] && accV[i][4] < AFP_y_limit[1])
          {
            p_reach = 1;
            passed = 1;
          }
          //else if (whichDetector.compare("ALFA")==0 && abs(accV[i][3]) < ALFA_x_limit[1] && abs(accV[i][4]) > ALFA_y_limit[0] && abs(accV[i][4]) < AFP_y_limit[1])
          else if (whichDetector.compare("ALFA")==0)
          {
            if (abs(accV[i][4]) > ALFA_y_limit[0] && abs(accV[i][4]) < ALFA_y_limit[1])
            {
              //if (abs(accV[i][3]) < ((ALFA_x_consts[0]*(ALFA_y_consts[0]+ALFA_y_consts[1]-abs(accV[i][4])))+(ALFA_x_consts[1]*(abs(accV[i][4])-ALFA_y_consts[0])))/(ALFA_y_consts[1]) )
              if (abs(accV[i][3]) <  ALFA_x_consts[1] - (((ALFA_y_consts[1]-(abs(accV[i][4])- ALFA_y_consts[0]))*(ALFA_x_consts[1]- ALFA_x_consts[0]))/ALFA_y_consts[1]) ) 
              {
                  p_reach++;
                  passed = 1;
              }  
            }
            else if (abs(accV[i][4]) > ALFA_y_limit[1] && abs(accV[i][4]) < ALFA_y_limit[2])
            {
              if (abs(accV[i][3]) < (0.0309 + ALFA_y_consts[0] - abs(accV[i][4]))) 
              {
                  p_reach++;
                  passed = 1;
              }    
            }
            //p_reach = 1;
          }
        }

	    }
      //if (i==accV.size()-1){ cout << "HERE!!!" << endl;
      //save2 << accV[i][0] << " " << accV[i][1] << " " << ((double)p_reach/(double)p_total)*100 << endl;}
      //cout << accV[i][3] << " :x--y: " << accV[i][4] <<endl;
      ellipse.push_back(accV[i][3]);
      ellipse.push_back(accV[i][4]);
      ellipse.push_back(passed);
      ellipses.push_back(ellipse);
      ellipse.clear();
	  }
	  save2.close();
  }
  //-----------------------------------------------------------------------------------------------------------
  //-----------------------------------------------------------------------------------------------------------
  //-----------------------------------------------------------------------------------------------------------
  TLatex *text = new TLatex();
  //----------------------------------------------------------------------
  if (std::ifstream(("./data/acceptance_"+fileName+".dat").c_str()))
  {
    TGraph2D *g1 = new TGraph2D(("./data/acceptance_"+fileName+".dat").c_str());
    TCanvas *c1 = new TCanvas("c1","c1",800,600);
    c1->cd();
    //cout << g1->GetN() << endl;
    g1->Draw("COLZ");
    g1->SetTitle(";proton transverse momentum #font[52]{p}_{T} [GeV/c];proton relative energy loss #font[52]{#xi};geometric acceptance [%]");

    c1->SetRightMargin(0.17);
    c1->SetLeftMargin(0.15);
    c1->Modified();
  
    ATLASLabel(0.2,0.87,atlaslabel,1);
    myText(0.2, 0.80, 1, ("#beta^{*}="+betaStar+"m, #theta_{C}="+crossingAngle+" #murad, #sqrt{s}="+CM+" TeV").c_str());
    if (whichStation.compare("NS")==0) myText(0.3, 0.96, 1, (whichDetector+" Near Station ("+to_string((int)round(observed_point))+"m)").c_str());
    else myText(0.3, 0.96, 1, (whichDetector+" Far Station ("+to_string((int)round(observed_point))+"m)").c_str());
    c1->Update();
    c1->Print(("./plots/acceptance_"+fileName+".pdf").c_str());
    c1->Close();
    delete g1;
  }
  else std::cout << "Acceptance file is missing!" << std::endl;
  //----------------------------------------------------------------------
  //----------------------------------------------------------------------
  if (save_beamtrajectory)
  {
  //----------------------------------------------------------------------
  if (std::ifstream(("./data/beamtrajectory_"+fileName+".dat").c_str()))
  {
    TGraph2D *g2 = new TGraph2D(("./data/beamtrajectory_"+fileName+".dat").c_str());
    TCanvas *c2 = new TCanvas("c2","c2",800,600);
    c2->cd();
    //cout << g2->GetN() << endl;
    g2->Draw("pcol");
    g2->SetTitle(";#font[52]{x}[m];#font[52]{y}[m];#font[52]{z}[m]");

    c2->SetRightMargin(0.17);
    c2->SetLeftMargin(0.15);
    c2->Modified();
    
    ATLASLabel(0.2,0.87,atlaslabel,1);
    myText(0.2, 0.80, 1, ("#beta^{*}="+betaStar+"m, #theta_{C}="+crossingAngle+" #murad, #sqrt{s}="+CM+" TeV").c_str());
    if (whichStation.compare("NS")==0) myText(0.3, 0.96, 1, (whichDetector+" Near Station ("+to_string((int)round(observed_point))+"m)").c_str());
    else myText(0.3, 0.96, 1, (whichDetector+" Far Station ("+to_string((int)round(observed_point))+"m)").c_str());
    c2->Update();
    c2->Print(("./plots/beamtrajectory_"+fileName+".pdf").c_str());
    c2->Close();
    delete g2;
    //----------------------------------------------------------------------
    TGraph *h21 = new TGraph();
    TGraph *h22 = new TGraph();
    //TH2D * h21 = new TH2D("h21","Beam Trajectory", 100,-0.001,0.001, 100,0,0.008);
    ifstream inputFile21;
    inputFile21.open(("./data/beamtrajectory_"+fileName+".dat").c_str());
    int count=0;
    while(!inputFile21.eof())
    {
  	double x, y, z;
  	inputFile21 >> x >> y >> z;
  	//h21->Fill(x,y,z);
  	h21->SetPoint(count,z,x*1000);
    h22->SetPoint(count,z,y*1000);
  	count++;
    }
    TCanvas *c21 = new TCanvas("c21","c21",800,600);
    c21->cd();
    h21->SetTitle(";#font[52]{z}[m];#font[52]{x}[mm]");
    h21->Draw("AP");

    c21->SetRightMargin(0.17);
    c21->SetLeftMargin(0.15);
    c21->Modified();
    
    ATLASLabel(0.2,0.87,atlaslabel,1);
    myText(0.2, 0.80, 1, ("#beta^{*}="+betaStar+"m, #theta_{C}="+crossingAngle+" #murad, #sqrt{s}="+CM+" TeV").c_str());
    if (whichStation.compare("NS")==0) myText(0.3, 0.96, 1, (whichDetector+" Near Station ("+to_string((int)round(observed_point))+"m)").c_str());
    else myText(0.3, 0.96, 1, (whichDetector+" Far Station ("+to_string((int)round(observed_point))+"m)").c_str());
    c21->Update();
    c21->Print(("./plots/beamtrajectory1D_"+fileName+"_X.pdf").c_str());
    c21->Close();
    delete h21;

    TCanvas *c22 = new TCanvas("c22","c22",800,600);
    c22->cd();
    h22->SetTitle(";#font[52]{z}[m];#font[52]{y}[mm]");
    h22->Draw("AP");

    c22->SetRightMargin(0.17);
    c22->SetLeftMargin(0.15);
    c22->Modified();
    ATLASLabel(0.2,0.87," Work in progress",1);
    //ATLASLabel(0.2,0.87," Simulation",1);
    myText(0.2, 0.80, 1, ("#beta^{*}="+betaStar+"m, #theta_{C}="+crossingAngle+" #murad, #sqrt{s}="+CM+" TeV").c_str());
    if (whichStation.compare("NS")==0) myText(0.3, 0.96, 1, (whichDetector+" Near Station ("+to_string((int)round(observed_point))+"m)").c_str());
    else myText(0.3, 0.96, 1, (whichDetector+" Far Station ("+to_string((int)round(observed_point))+"m)").c_str());
    c22->Update();
    c22->Print(("./plots/beamtrajectory1D_"+fileName+"_Y.pdf").c_str());
    c22->Close();
    delete h22;
  }
  else std::cout << "Beam trajectory file is missing!" << std::endl;
  //----------------------------------------------------------------------
  }
  //----------------------------------------------------------------------
  if (save_acceptance) 
  {
  //----------------------------------------------------------------------
  std::vector<std::vector<double>> beam_width = p->GetBeamWidth();

  TH2F * h2 = new TH2F("h2","Beam Width X", 337,0,520, 337,0,0.5);
  for (int i=0;i<beam_width.size();i++) h2->Fill(beam_width[i][2],beam_width[i][0]);
  TCanvas* c3=new TCanvas("c3","c3",800,600);
  c3->cd();
  h2->GetXaxis()->SetTitle("#font[52]{z}[m]");
  h2->GetYaxis()->SetTitle("#font[52]{#sigma_{x}} [mm]");
  h2->Draw("BOX");

  //c3->SetRightMargin(0.17);
  c3->SetLeftMargin(0.15);
  c3->Modified();

  ATLASLabel(0.2,0.87,atlaslabel,1);
  c3->Update();
  
  c3->Print(("./plots/beamwidth_"+fileName+"_X.pdf").c_str());
  c3->Close();
  delete h2;
  //----------------------------------------------------------------------
  TH2F * h3 = new TH2F("h3","Beam Width Y", 337,0,520, 337,0,0.5);
  for (int i=0;i<beam_width.size();i++) h3->Fill(beam_width[i][2],beam_width[i][1]);
  TCanvas* c4=new TCanvas("c3","c3",800,600);
  c4->cd();
  h3->GetXaxis()->SetTitle("#font[52]{z}[m]");
  h3->GetYaxis()->SetTitle("#font[52]{#sigma_{y}} [mm]");
  h3->Draw("BOX");

  //c4->SetRightMargin(0.17);
  c4->SetLeftMargin(0.15);
  c4->Modified();
  
  ATLASLabel(0.2,0.87,atlaslabel,1);
  c4->Update();
  
  c4->Print(("./plots/beamwidth_"+fileName+"_Y.pdf").c_str());
  c4->Close();
  delete h3;
  //----------------------------------------------------------------------
  TH2F * h4 = new TH2F("h4","h4", 1000,-23,23, 100,-31,31);
  TH2F * h5 = new TH2F("h5","h5", 1000,-23,23, 100,-31,31);
  for (int i=0;i<ellipses.size();i++)
  {
    h4->Fill(ellipses[i][0]*1000,ellipses[i][1]*1000,ellipses[i][2]);
    //if (ellipses[i][2]-1==0) h4->Fill(ellipses[i][0]*1000,ellipses[i][1]*1000,ellipses[i][2]);
    //else h5->Fill(ellipses[i][0]*1000,ellipses[i][1]*1000,ellipses[i][2]);
  } 
  TCanvas* c5=new TCanvas("c5","c5",800,600);
  c5->cd();
  h4->GetXaxis()->SetTitle("#font[52]{x}[mm]");
  h4->GetYaxis()->SetTitle("#font[52]{y}[mm]");
  h4->SetMarkerColor(4);
  h4->SetMarkerSize(3);
  h5->SetMarkerColor(2);
  h5->SetMarkerSize(3);
  h4->Draw("COLZ1");
  //h5->Draw("SAME COLZ1");

  //c5->SetRightMargin(0.17);
  c5->SetLeftMargin(0.15);
  c5->Modified();
  
  ATLASLabel(0.2,0.87,atlaslabel,1);
  c5->Update();
  
  c5->Print(("./plots/ellipses_"+fileName+".pdf").c_str());
  c5->Close();
  delete h4;
  delete h5;
  //----------------------------------------------------------------------
  }
  //----------------------------------------------------------------------
  //----------------------------------------------------------------------

  delete p;
  }}}//For loops closed.
  }//For the loop BD_MM
  return 0;
}

void ProtonTransport(){

  main();
}
