/**
 \author Maciej Trzebinski
 \author Yusuf Can Cekmecelioglu
 \version 3.0
 \date 21/11/2022
*/


#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cmath>
#include <unistd.h>

class ProtonTransport {
  public:
    ProtonTransport(); //!< constructor 
    ~ProtonTransport(); //!< destructor 
    
    void SimpleTracking(double, double, double, double, double, double, double, bool, bool);
    
    void NominalTrajectory(double);
    
    void PrepareBeamline(std::string, bool); 
    void SetBeamEnergy(double);
    double GetBeamEnergy() const;
    void SetCrossingAngle(double);
    double GetCrossingAngle() const;
    void SetCrossingAnglePhi(double);
    double GetCrossingAnglePhi() const;
    void SetBeampipeSeparation(double);
    double GetBeampipeSeparation() const;
    double GetLostPos() const;
    
    void SetEmittance(double);
    double GetEmittance() const;
    
    void SetDoApertureCut(bool);
    bool GetDoApertureCut() const;
    
    void SetTCL4Jaw(double);
    double GetTCL4Jaw() const;
    void SetTCL5Jaw(double);
    double GetTCL5Jaw() const;
    void SetTCL6Jaw(double);
    double GetTCL6Jaw() const;
    void SetTCL4Pos(double);
    double GetTCL4Pos() const;
    void SetTCL5Pos(double);
    double GetTCL5Pos() const;
    void SetTCL6Pos(double);
    double GetTCL6Pos() const;
    void SetTCL4Center(double);
    double GetTCL4Center() const;
    void SetTCL5Center(double);
    double GetTCL5Center() const;
    void SetTCL6Center(double);
    double GetTCL6Center() const;
    void SetTCL4Sigma(double);
    double GetTCL4Sigma() const;
    void SetTCL5Sigma(double);
    double GetTCL5Sigma() const;
    void SetTCL6Sigma(double);
    double GetTCL6Sigma() const;
    
    double GetX() const;
    void SetX(double);
    double GetY() const;
    void SetY(double);
    double GetZ() const;
    void SetZ(double);
    double GetPx() const;
    void SetPx(double);
    double GetPy() const;
    void SetPy(double);
    double GetPz() const;
    void SetPz(double);
    double GetSx() const;
    void SetSx(double);
    double GetSy() const;
    void SetSy(double);
    
    std::vector<std::vector<double>> GetBeamWidth();
    std::vector<std::vector<double>> GetBeamTrajectory();
    std::vector<double> GetBeamCenter();
    
    
  private:
    double IP1Pos;
    double x, y, z, px, py, pz, sx, sy;
    double beam_energy;
    double emittance;
    double crossing_angle;
    double crossing_angle_phi;
    bool BeampipesAreSeparated;
    double BeampipeSeparation;
    void Marker(bool);
    void simple_drift(double, bool);
    void simple_rectangular_dipole(double, double, bool);
    void simple_horizontal_kicker(double, double, bool);
    void simple_vertical_kicker(double, double, bool);
    void simple_quadrupole(double, double, bool);
    void simple_collimator(double, bool);
    bool aperture_hit(int, bool);
    std::vector <std::vector <std::string> > element;
    bool DoApertureCut;
    double sigma_x;
    double sigma_y;
//    void SetBeamWidth(double);
//    double GetBeamWidth();
    
    double TCLJaw[3];
    double TCLPos[3];
    double TCLCenter[3];
    double TCLSigma[3];
    
    double gamma_rel = (6.5e12/938e6);
    std::vector <double> betax;
    std::vector <double> betay;
    std::vector <std::vector<double>> beam_width;
    std::vector <std::vector<double>> beam_trajectory;
    std::vector <double> beam_center;
    double proton_lost_pos;
    bool compute_sigma;
};

/** \class ProtonTransport
\brief All tools needed for proton transport through LHC structures.

Class constructor sets the following default values: \n 
beam_energy of 6500 \n
crossing_agnel of 0 \n 
BeampipesAreSeparated = false \n 
BeampipeSeparation of 97.e-3 \n 
DoApertureCut = true \n
Collimaotrs are wide open by default and centered at 0. Their certers are at 0 too -> this has to be checked with a nominal trajectory.\n
If betax and betay are provided, beam width at collimator location will be automatically computed;
*/
ProtonTransport::ProtonTransport() :
  beam_energy(6500.),
  emittance(3.5),
  crossing_angle(0.),
  crossing_angle_phi(90.),
  BeampipesAreSeparated(false),
  BeampipeSeparation(97.e-3),
  DoApertureCut(true),
  TCLJaw{999., 999., 999.},
  TCLPos{150.53-1., 184.857-1., 219.513-1.},
  TCLCenter{0., 0., 0.},
  TCLSigma{15., 40., 20.},
  proton_lost_pos(-1.),
  compute_sigma(true)
{
}


ProtonTransport::~ProtonTransport()
{
}


/**
\brief Set beam energy in GeV.

Must be accordingly to settings used in optics file.
\param[in] E value of beam energy in GeV.
*/
void ProtonTransport::SetBeamEnergy(double E){
  beam_energy = E;
}

/**
\brief Return beam energy in GeV.

\return beam_energy
*/
double ProtonTransport::GetBeamEnergy() const {
  return beam_energy;
}

/**
\brief Set crossing angle in rad.

Must be accordingly to settings used in optics file.
\param[in] angle value of half crossing-angle in rad.
*/
void ProtonTransport::SetCrossingAngle(double angle){
  crossing_angle = angle;
}

/**
\brief Return crossing angle in rad.

\return crossing_angle
*/
double ProtonTransport::GetCrossingAngle() const {
  return crossing_angle;
}

/**
\brief Set phase of crossing angle in degrees.

Must be accordingly to settings used in optics file.
\param[in] angle value of half crossing-angle in degrees. 0 is horizontal crossing angle (setting fro Run4), whereas 90 is vertical (run 2 and run 3)
*/
void ProtonTransport::SetCrossingAnglePhi(double angle){
  crossing_angle_phi = angle;
}

/**
\brief Return crossing angle phase in deg.

\return crossing_angle
*/
double ProtonTransport::GetCrossingAnglePhi() const {
  return crossing_angle_phi;
}

/**
\brief Set beam separation in m.

In vicinity of collision point protons are in one, common beampipe.
Splitting into two beampipes at TAN element, just before second dipole.
The nominal separation is 97.e-3 m i.e. 97 mm.

\param[in] d vaule of beam separation in m
*/
void ProtonTransport::SetBeampipeSeparation(double d){
  BeampipeSeparation = d;
}

/**
\brief Get beam separation in m.

\return BeampipeSeparation
*/
double ProtonTransport::GetBeampipeSeparation() const {
  return BeampipeSeparation;
}

/**
\brief Beam element - marker.

Marker is a "dummy" beam element used to mark a certain place between elements (in drift).
*/
void ProtonTransport::Marker(bool verbose=false){
  if (!verbose) return;
  std::cout << "MARKER\t";
  std::cout << "z [m]: " << z;
  std::cout << "\tx [mm]: " << x*1.e3; 
  std::cout << "\ty [mm]: " << y*1.e3;
  std::cout << "\tpx [GeV]: " << px;
  std::cout << "\tpy [GeV]: " << py;
  std::cout << "\tpz [GeV]: " << pz;
  std::cout << "\tsx: " << sx;
  std::cout << "\tsy: " << sy << std::endl;
}  

/**
\brief Beam element - drift.

Drift is just a beampipe section w/o any equipment - particle trajectory is a straight line.
*/
void ProtonTransport::simple_drift(double L, bool verbose=false){
  x+=L*sx;
  y+=L*sy;
  z+=L;
  if (!verbose) return;
  std::cout << "DRIFT\t";
  std::cout << "z [m]: " << z;
  std::cout << "\tx [mm]: " << x*1.e3; 
  std::cout << "\ty [mm]: " << y*1.e3;
  std::cout << "\tpx [GeV]: " << px;
  std::cout << "\tpy [GeV]: " << py;
  std::cout << "\tpz [GeV]: " << pz;
  std::cout << "\tsx: " << sx;
  std::cout << "\tsy: " << sy << std::endl;
}

/**
\brief Beam element - simple rectangular dipole.

Bent is in x: x += L*sx + L*0.5*K0L*beam_energy/pz, i.e. length * initial slope + length * half of angle (from geometry) * correction due to energy loss
There is also change of angle: sx += K0L*beam_energy/pz;
*/
void ProtonTransport::simple_rectangular_dipole(double L, double K0L, bool verbose = false){

  if (fabs(K0L) < 1.e-15)
  {
    simple_drift(L, verbose);
    return;
  }
  z += L;
  x += L*sx + L*0.5*K0L*beam_energy/pz; // length * initial slope + length * half of angle (from geometry) * correction due to energy loss
  y += L*sy;
  sx += K0L*beam_energy/pz;
  px = sx*pz;
  //sy does not change
  if (!verbose) return;
  std::cout << "RECTANGULAR DIPOLE\t";
  std::cout << "z [m]: " << z;
  std::cout << "\tx [mm]: " << x*1.e3; 
  std::cout << "\ty [mm]: " << y*1.e3;
  std::cout << "\tpx [GeV]: " << px;
  std::cout << "\tpy [GeV]: " << py;
  std::cout << "\tpz [GeV]: " << pz;
  std::cout << "\tsx: " << sx;
  std::cout << "\tsy: " << sy << std::endl;
}

/**
\brief Beam element -- simple horizontal kicker

Bending power is like in dipole: x += L*sx + L*0.5*HKICK*beam_energy/pz; // length * initial slope + length * half of angle (from geometry) * correction due to energy loss \n
but std::since kickers often has L=0, only angle is influenced:
sx += HKICK*beam_energy/pz;
*/
void ProtonTransport::simple_horizontal_kicker(double L, double HKICK, bool verbose = false){
  if (fabs(HKICK) < 1.e-15)
  {
    simple_drift(L, verbose);
    return;
  }
  z += L;
  x += L*sx - L*0.5*HKICK*beam_energy/pz; // length * initial slope + length * half of angle (from geometry) * correction due to energy loss
  y += L*sy;
  sx -= HKICK*beam_energy/pz;
  px = sx*pz;
  if (!verbose) return;
  std::cout << "HORIZONTAL KICKER\t";
  std::cout << "z [m]: " << z;
  std::cout << "\tx [mm]: " << x*1.e3; 
  std::cout << "\ty [mm]: " << y*1.e3;
  std::cout << "\tpx [GeV]: " << px;
  std::cout << "\tpy [GeV]: " << py;
  std::cout << "\tpz [GeV]: " << pz;
  std::cout << "\tsx: " << sx;
  std::cout << "\tsy: " << sy << std::endl;
}


/**
\brief Beam element -- simple vertical kicker

Bending power is like in dipole: y += L*sy + L*0.5*HKICK*beam_energy/pz; // length * initial slope + length * half of angle (from geometry) * correction due to energy loss \n
but std::since kickers often has L=0, only angle is influenced:
sy += HKICK*beam_energy/pz;
*/
void ProtonTransport::simple_vertical_kicker(double L, double VKICK, bool verbose = false){
  if (fabs(VKICK) < 1.e-15)
  {
    simple_drift(L, verbose);
    return;
  }
  z += L;
  x += L*sx;
  y += L*sy + L*0.5*VKICK*beam_energy/pz; // length * initial slope + length * half of angle (from geometry) * correction due to energy loss
  sy += VKICK*beam_energy/pz;
  py = sy*pz;
  if (!verbose) return;
  std::cout << "VERTICAL KICKER\t";
  std::cout << "z [m]: " << z;
  std::cout << "\tx [mm]: " << x*1.e3; 
  std::cout << "\ty [mm]: " << y*1.e3;
  std::cout << "\tpx [GeV]: " << px;
  std::cout << "\tpy [GeV]: " << py;
  std::cout << "\tpz [GeV]: " << pz;
  std::cout << "\tsx: " << sx;
  std::cout << "\tsy: " << sy << std::endl;
}

/**
\brief Beam element -- simple quadrupole

Focucrossing depends on sign of K1L value, if positive than horizontal focucrossing, else vertical focucrossing. \n
*/
void ProtonTransport::simple_quadrupole(double L, double K1L, bool verbose=false){
  if (fabs(K1L) < 1.e-15)
  {
    simple_drift(L, verbose);
    return;
  }
  z += L;
  double qk  = std::sqrt((fabs(K1L) * beam_energy)/(pz * L));
  double qkl = qk * L;
  double x_tmp = x;
  double y_tmp = y;

  if (K1L >= 0.) //horizontal focucrossing
  {
    fabs(x)  > 1.e-15 ? x =  std::cos(qkl) * x       : x = 0.;
    fabs(sx) > 1.e-15 ? x += std::sin(qkl) * sx / qk : x += 0.;

    fabs(y)  > 1.e-15 ? y = std::cosh(qkl) * y        : y = 0.;
    fabs(sy) > 1.e-15 ? y += std::sinh(qkl) * sy / qk : y += 0.;

    fabs(sx) > 1.e-15 ? sx = std::cos(qkl) * sx           : sx = 0.;
    fabs(x_tmp)  > 1.e-15 ? sx += -qk * std::sin(qkl) * x_tmp : sx += 0.;

    fabs(sy) > 1.e-15 ? sy = std::cosh(qkl) * sy          : sy = 0.;
    fabs(y_tmp)  > 1.e-15 ? sy += qk * std::sinh(qkl) * y_tmp : sy += 0.;
  }
  else //vertical focucrossing
  {
    fabs(y)  > 1.e-15 ? y =  std::cos(qkl) * y       : y = 0.;
    fabs(sy) > 1.e-15 ? y += std::sin(qkl) * sy / qk : y += 0.;

    fabs(x)  > 1.e-15 ? x = std::cosh(qkl) * x        : x = 0.;
    fabs(sx) > 1.e-15 ? x += std::sinh(qkl) * sx / qk : x += 0.;

    fabs(sy) > 1.e-15 ? sy = std::cos(qkl) * sy           : sy = 0.;
    fabs(y_tmp)  > 1.e-15 ? sy += -qk * std::sin(qkl) * y_tmp : sy += 0.;

    fabs(sx) > 1.e-15 ? sx = std::cosh(qkl) * sx          : sx = 0.;
    fabs(x_tmp)  > 1.e-15 ? sx += qk * std::sinh(qkl) * x_tmp : sx += 0.;
  }
  px = sx*pz;
  py = sy*pz;
  if (!verbose) return;
  std::cout << "QUADRUPOLE\t";
  std::cout << "z [m]: " << z;
  std::cout << "\tx [mm]: " << x*1.e3; 
  std::cout << "\ty [mm]: " << y*1.e3;
  std::cout << "\tpx [GeV]: " << px;
  std::cout << "\tpy [GeV]: " << py;
  std::cout << "\tpz [GeV]: " << pz;
  std::cout << "\tsx: " << sx;
  std::cout << "\tsy: " << sy << std::endl;
}

/**
\brief Beam element - simple collimator

First movable collimator to be considered is TCL 4. Therefore, all elements before 130 m (TAS & TAN) are considered as drift with cut only on their aperture.\\
Collimator positions are set via SetTCLXPos(double) where x = 3, 4 or 5. \n
Collimator jaw closure should be centered around nominal beam trajectory instead of beampipe center. This "shift" should be included in SetTCLXCenter().

TODO 0.0129796
*/
void ProtonTransport::simple_collimator(double L, bool verbose = false){
  if (z < 130.) {simple_drift(L,verbose); return;}
  
  if (fabs(z - GetTCL4Pos()) < 0.1) //TCL4
  {
    if ((x < GetTCL4Center() - GetTCL4Jaw()) && (proton_lost_pos < 0.)) proton_lost_pos = z;
    simple_drift(L, verbose);
  }
  else if (fabs(z - GetTCL5Pos()) < 0.1) //TCL5
  {
    if ((x < GetTCL5Center() - GetTCL5Jaw()) && (proton_lost_pos < 0.)) proton_lost_pos = z;
    simple_drift(L, verbose);
  }
  else if (fabs(z - GetTCL6Pos()) < 0.1) //TCL6
  {
    if ((x < GetTCL6Center() - GetTCL6Jaw()) && (proton_lost_pos < 0.)) proton_lost_pos = z;
    simple_drift(L, verbose);
  }
  else //other collimators; e.g. in front of quadrupoles for Run 4 (TCLMC*)
  {
    simple_drift(L);
  }
  if (!verbose) return;
  std::cout << "COLLIMATOR\t";
  std::cout << "z [m]: " << z;
  std::cout << "\tx [mm]: " << x*1.e3; 
  std::cout << "\ty [mm]: " << y*1.e3;
  std::cout << "\tpx [GeV]: " << px;
  std::cout << "\tpy [GeV]: " << py;
  std::cout << "\tpz [GeV]: " << pz;
  std::cout << "\tsx: " << sx;
  std::cout << "\tsy: " << sy;
  std::cout << "\tproton_lost_pos: " << proton_lost_pos << std::endl;
}


/**
\brief Beam element - aperture

Apertures are usually defined in the Twiss files. The following aperture types are implemented:\n\n
APERTYPE 	# of values 	meaning of values\n
CIRCLE 	1 	radius of circle\n
RECTANGLE 	2 	half width and half height of rectangle\n
ELLIPSE 	2 	horizontal and vertical semi-axes of ellipse\n
RECTCIRCLE or LHCSCREEN 	3 	half width and half height of rectangle, radius of circle,\n
RECTELLIPSE 	4 	half width and half height of rectangle, horizontal and vertical semi-axes of ellipse,\n
OCTAGON 	4 	half width and half height along main axes, two angles sustaining the cut corner in the first quadrant, given in radians and in order of increastd::sing values.
*/
bool ProtonTransport::aperture_hit(int a, bool verbose = false)
{
  if ((element[a].at(9)).compare("\"NONE\"") == 0)
  {
    if (verbose) std::cout << "APERTURE: NONE" << std::endl;
    return false;
  }
  
  if ((element[a].at(9)).compare("\"CIRCLE\"") == 0)
  {
    if (verbose) std::cout << "APERTURE: CIRCLE\tr: " << std::sqrt(x*x + y*y) << "\t r_aper: " << stod(element[a].at(10)) << std::endl;
    if (stod(element[a].at(10)) < 1.e-10) return false; // some circles have radious 0...
    if (std::sqrt(x*x + y*y) > stod(element[a].at(10))) {if (proton_lost_pos < 0.) proton_lost_pos = z; return true;}
    else return false;
  }
  
  if ((element[a].at(9)).compare("\"RECTANGLE\"") == 0)
  {
    if (verbose) std::cout << "APERTURE: RECTANGLE\tx: " << x << "\t x_aper: " << stod(element[a].at(10)) << "\ty: " << x << "\t y_aper: " << stod(element[a].at(11)) << std::endl;
    if (stod(element[a].at(10)) < 1.e-10) return false; // in case of 0 size...
    if ( (fabs(x) > stod(element[a].at(10))) || (fabs(y) > stod(element[a].at(11))) ) {if (proton_lost_pos < 0.) proton_lost_pos = z; return true;}
    else return false;
  }
  
  if ((element[a].at(9)).compare("\"ELLIPSE\"") == 0)
  {
    if (verbose) std::cout << "APERTURE: ELLIPSE\tx^2/x_aper^2: " << x*x/(stod(element[a].at(10))*stod(element[a].at(10))) << "\t y^2/y_aper^2: " << y*y/(stod(element[a].at(11))*stod(element[a].at(11))) << std::endl;
    if (stod(element[a].at(10)) < 1.e-10) return false; // in case of 0 size...
    if ( ((x*x/(stod(element[a].at(10))*stod(element[a].at(10))) + y*y/(stod(element[a].at(11))*stod(element[a].at(11)))) > 1.) ) {if (proton_lost_pos < 0.) proton_lost_pos = z; return true;}
    else return false;
  }
  
  if ( ((element[a].at(9)).compare("\"RECTCIRCLE\"") == 0) || ((element[a].at(9)).compare("\"LHCSCREEN\"") == 0) )
  {
    if (verbose) std::cout << "APERTURE: RECTCIRCLE or LHCSCREEN\tx: " << x << "\t x_aper: " << stod(element[a].at(10)) << "\ty: " << x << "\t y_aper: " << stod(element[a].at(11)) << "\tr: " << std::sqrt(x*x + y*y) << "\t r_aper: " << stod(element[a].at(12)) <<std::endl;
    if (stod(element[a].at(10)) < 1.e-10) return false; // in case of 0 size...
    if ( (fabs(x) > stod(element[a].at(10))) || (fabs(y) > stod(element[a].at(11))) || (std::sqrt(x*x + y*y) > stod(element[a].at(12))) ) {if (proton_lost_pos < 0.) proton_lost_pos = z; return true;}
    else return false;
  }
  
  if ((element[a].at(9)).compare("\"RECTELLIPSE\"") == 0)
  {
    if (verbose) std::cout << "APERTURE: RECTELLIPSE\tx: " << x << "\t x_aper: " << stod(element[a].at(10)) << "\ty: " << x << "\t y_aper: " << stod(element[a].at(11)) << "\tx^2/x_aper^2: " << x*x/(stod(element[a].at(12))*stod(element[a].at(12))) << "\t y^2/y_aper^2: " << y*y/(stod(element[a].at(13))*stod(element[a].at(13))) << std::endl;
    if (stod(element[a].at(10)) < 1.e-10) return false; // in case of 0 size...
    if ( (fabs(x) > stod(element[a].at(10))) || (fabs(y) > stod(element[a].at(11))) || ((x*x/(stod(element[a].at(12))*stod(element[a].at(12))) + y*y/(stod(element[a].at(13))*stod(element[a].at(13)))) > 1.) ) {if (proton_lost_pos < 0.) proton_lost_pos = z; return true;}
    else return false;
  }
  
  if ((element[a].at(9)).compare("\"OCTAGON\"") == 0)
  {
    // first, a cut on rectangle:
 //   std::cout << z << "\t" << x << "\t" << y << std::endl;
    if (verbose) std::cout << "APERTURE: OCTAGON (rectangle part)\tx: " << x << "\t x_aper: " << stod(element[a].at(10)) << "\ty: " << x << "\t y_aper: " << stod(element[a].at(11)) << std::endl;
    if ( (fabs(x) > stod(element[a].at(10))) || (fabs(y) > stod(element[a].at(11))) ) {if (proton_lost_pos < 0.) proton_lost_pos = z; return true;}
    // next, compute "corner"
 /*   double tmp_x3 = stod(element[a].at(10));
    double tmp_y3 = stod(element[a].at(10)) * tan(stod(element[a].at(12)));
    double tmp_x4 = stod(element[a].at(11)) / tan(stod(element[a].at(13)));
    double tmp_y4 = stod(element[a].at(11));
    double tmp_A  = (tmp_y3 - tmp_y4)/(tmp_x4 - tmp_x3);
    double tmp_B  = tmp_y4 - tmp_x3 * (tmp_y3 - tmp_y4) / (tmp_x4 - tmp_x3);
    
    if (verbose) std::cout << "APERTURE: OCTAGON (corner part)\t|y|: " << fabs(y) << "\tA * |x| + B: " << tmp_A * fabs(x) + tmp_B << std::endl;
    
    if ( fabs(y) > tmp_A * fabs(x) + tmp_B ) {if (proton_lost_pos < 0.) proton_lost_pos = z; return true;}
    else return false;*/
    double slope = ( stod(element[a].at(11)) - stod(element[a].at(10)) * tan(stod(element[a].at(12))) )/( stod(element[a].at(11))/tan(stod(element[a].at(13))) - stod(element[a].at(10)) );
    if ( fabs(y) > slope * (fabs(x) - stod(element[a].at(11))/tan(stod(element[a].at(13)))) + stod(element[a].at(11)) ) {if (proton_lost_pos < 0.) proton_lost_pos = z; return true;}
    else return false;
  }
  
  std::cout << "WARNING!!! In aperture_hit the following element is not defined:\t" << element[a].at(9) << std::endl;

  return true;
}

/**
\brief Setting emittance in mum*rad.
*/
void ProtonTransport::SetEmittance(double em){emittance = em;}

/**
\brief Getting the emittance in mum*rad.
*/
double ProtonTransport::GetEmittance() const {return emittance;}

/**
\brief Setting DoApertureCut value (bool).
*/
void ProtonTransport::SetDoApertureCut(bool val){DoApertureCut = val;}

/**
\brief Getting DoApertureCut value.
*/
bool ProtonTransport::GetDoApertureCut() const {return DoApertureCut;}

/**
\brief Setting the opening of TCL4 jaw in m.
*/
void ProtonTransport::SetTCL4Jaw(double jaw){TCLJaw[0] = jaw;}

/**
\brief Getting the opening of TCL4 jaw in m.
*/
double ProtonTransport::GetTCL4Jaw() const {return TCLJaw[0];}

/**
\brief Setting the opening of TCL5 jaw in m.
*/
void ProtonTransport::SetTCL5Jaw(double jaw){TCLJaw[1] = jaw;}

/**
\brief Getting the opening of TCL5 jaw in m.
*/
double ProtonTransport::GetTCL5Jaw() const {return TCLJaw[1];}

/**
\brief Setting the opening of TCL6 jaw in m.
*/
void ProtonTransport::SetTCL6Jaw(double jaw){TCLJaw[2] = jaw;}

/**
\brief Getting the opening of TCL6 jaw in m.
*/
double ProtonTransport::GetTCL6Jaw() const {return TCLJaw[2];}

/**
\brief Setting position of TCL4 along beam in m.
*/
void ProtonTransport::SetTCL4Pos(double pos){TCLPos[0] = pos;}

/**
\brief Getting position of TCL4 along beam in m.
*/
double ProtonTransport::GetTCL4Pos() const {return TCLPos[0];}

/**
\brief Setting position of TCL5 along beam in m.
*/
void ProtonTransport::SetTCL5Pos(double pos){TCLPos[1] = pos;}

/**
\brief Getting position of TCL5 along beam in m.
*/
double ProtonTransport::GetTCL5Pos() const {return TCLPos[1];}

/**
\brief Setting position of TCL6 along beam in m.
*/
void ProtonTransport::SetTCL6Pos(double pos){TCLPos[2] = pos;}

/**
\brief Getting position of TCL6 along beam in m.
*/
double ProtonTransport::GetTCL6Pos() const {return TCLPos[2];}

/**
\brief Setting center of beam at TCL4 location in m. Should be obtaind from checking nominal proton trajectory for given optics.
*/
void ProtonTransport::SetTCL4Center(double center){TCLCenter[0] = center;}

/**
\brief Getting center of beam at TCL4 location in m.
*/
double ProtonTransport::GetTCL4Center() const {return TCLCenter[0];}

/**
\brief Setting center of beam at TCL5 location in m. Should be obtaind from checking nominal proton trajectory for given optics.
*/
void ProtonTransport::SetTCL5Center(double center){TCLCenter[1] = center;}

/**
\brief Getting center of beam at TCL5 location in m.
*/
double ProtonTransport::GetTCL5Center() const {return TCLCenter[1];}

/**
\brief Setting center of beam at TCL6 location in m. Should be obtaind from checking nominal proton trajectory for given optics.
*/
void ProtonTransport::SetTCL6Center(double center){TCLCenter[2] = center;}

/**
\brief Getting center of beam at TCL6 location in m.
*/
double ProtonTransport::GetTCL6Center() const {return TCLCenter[2];}


/**
\brief Setting opening of TCL4 in sigma.
*/
void ProtonTransport::SetTCL4Sigma(double sigma){TCLSigma[0] = sigma;}

/**
\brief Getting opening of TCL4 in sigma
*/
double ProtonTransport::GetTCL4Sigma() const {return TCLSigma[0];}

/**
\brief Setting opening of TCL5 in sigma
*/
void ProtonTransport::SetTCL5Sigma(double sigma){TCLSigma[1] = sigma;}

/**
\brief Getting opening of TCL5 in sigma
*/
double ProtonTransport::GetTCL5Sigma() const {return TCLSigma[1];}

/**
\brief Setting opening of TCL5 in sigma
*/
void ProtonTransport::SetTCL6Sigma(double sigma){TCLSigma[2] = sigma;}

/**
\brief Getting opening of TCL5 in sigma
*/
double ProtonTransport::GetTCL6Sigma() const {return TCLSigma[2];}

/**
\brief Get x in m.
*/
double ProtonTransport::GetX() const {return x;}

/**
\brief Set x in m.
*/
void ProtonTransport::SetX(double var){x = var;}

/**
\brief Get y in m.
*/
double ProtonTransport::GetY() const {return y;}

/**
\brief Set y in m.
*/
void ProtonTransport::SetY(double var){y = var;}

/**
\brief Get z in m.
*/
double ProtonTransport::GetZ() const {return z;}

/**
\brief Set z in m.
*/
void ProtonTransport::SetZ(double var){z = var;}

/**
\brief Get px in GeV.
*/
double ProtonTransport::GetPx() const {return px;}

/**
\brief Set px in GeV.
*/
void ProtonTransport::SetPx(double var){px = var;}

/**
\brief Get py in GeV.
*/
double ProtonTransport::GetPy() const {return py;}

/**
\brief Set py in GeV.
*/
void ProtonTransport::SetPy(double var){py = var;}

/**
\brief Get pz in GeV.
*/
double ProtonTransport::GetPz() const {return pz;}

/**
\brief Set pz in GeV.
*/
void ProtonTransport::SetPz(double var){pz = var;}

/**
\brief Get sx in rad.
*/
double ProtonTransport::GetSx() const {return sx;}

/**
\brief Set sx in rad.
*/
void ProtonTransport::SetSx(double var){sx = var;}

/**
\brief Get y in rad.
*/
double ProtonTransport::GetSy() const {return sy;}

/**
\brief Set sy in rad.
*/
void ProtonTransport::SetSy(double var){sy = var;}

/**
\brief Get position at which proton was lost in m.
*/
double ProtonTransport::GetLostPos() const {return proton_lost_pos;}

/**
\brief Get beam width in mm.
*/
std::vector<std::vector<double>> ProtonTransport::GetBeamWidth() {return beam_width;}

/**
\brief Get beam trajectory in mm.
*/
std::vector<std::vector<double>> ProtonTransport::GetBeamTrajectory() {return beam_trajectory;}

/**
\brief Get beam center as {x,y,beta_x}.
*/
std::vector<double> ProtonTransport::GetBeamCenter() {return beam_center;}

/**
\brief Extract beam elements from twiss file.

The following variables are xepected to be present in TWiss file. The order in twiss files does not matter.

Mandatory:
- KEYWORD
- S
- L
- HKICK
- VKICK
- K0L
- K1L
- K2L
- K3L

If one of these is not provided, the aperture will NOT be considered:
- APERTYPE
- APER_1
- APER_2
- APER_3
- APER_4

If one of these is not provided, the beam width will not be computed:
- BETX
- BETY

These values are optional. For now they are not used in the tracking code. Still, if they are missing a warning will appear.
- X
- Y
- PX
- PY
*/
void ProtonTransport::PrepareBeamline(std::string filename, bool verbose=false){

  std::vector <std::string> sorted_param; //type, S, L, HKICK, VKICK, K0L, K1L, K2L, K3L, APERTYPE, APER_1, APER_2, APER_3, APER_4, X, Y, PX, PY
  std::vector <std::string> unsorted_name;
  std::vector <std::string> unsorted_param;

  int n_elements = 20;
  int sorting_order[n_elements];
  for (int a=0; a<n_elements; a++) sorting_order[a] = 0;
  //std::string sorting_order_names[n_elements] = {"KEYWORD", "S", "L", "HKICK", "VKICK", "K0L", "K1L", "K2L", "K3L", "APERTYPE", "APER_1", "APER_2", "APER_3", "APER_4", "BETX", "BETY", "X", "Y", "PX", "PY"};
  std::vector<std::string> sorting_order_names {"KEYWORD", "S", "L", "HKICK", "VKICK", "K0L", "K1L", "K2L", "K3L", "APERTYPE", "APER_1", "APER_2", "APER_3", "APER_4", "BETX", "BETY", "X", "Y", "PX", "PY"};

  bool IsIP1 = false;

  std::ifstream in;
  in.open(filename.c_str());
  if (access(filename.c_str(), F_OK)) {std::cout << "ERROR! No file named: " << filename << std::endl; return;}
  
  while (!in.eof())
  {
    std::string line;
    if (in.peek() == 64) //64 is '@' symbol
    {
      //these lines are comments, not used for now in the code so can be skipped
      in.ignore(5000, '\n');
      continue;
    }
    if (in.peek() == 42) //42 is '*' symbol
    {
      //this line contains names of variables; presence/absence of variables can differ between twiss files as order depends on parameters given at MAD-X generation step
      getline(in, line);
      std::istringstream ss(line);
      for (std::string keyword; ss >> keyword; ) unsorted_name.push_back(keyword);
    
      for (unsigned int a = 0; a<unsorted_name.size(); a++) 
        for (int b=0; b<n_elements; b++)
          if ((unsorted_name.at(a)).compare(sorting_order_names[b]) == 0) sorting_order[b] = a-1; // -1 because description starts with '*' symbol whereas values does not have it

      for (int a=0; a<n_elements; a++)
      {
        if ((a < 9) && sorting_order[a] == 0) {std::cout << "ERROR! Key element: " << sorting_order_names[a] << " is missing in Twiss file!" << std::endl; return;}
        if ((a >= 9) && (a < 14) && sorting_order[a] == 0) {std::cout << "WARNING! Information about aperture is missing. Will not be considered." << std::endl; DoApertureCut = false;}
        if ((a >= 14) && (a < 16) && sorting_order[a] == 0) {std::cout << "WARNING! Information about " << sorting_order_names[a] << " is missing. Beam widths will not be automatically computed." << std::endl; compute_sigma = false;}
        if ((a >= 16) && (a < 18) && sorting_order[a] == 0) {std::cout << "WARNING! Information about " << sorting_order_names[a] << " is missing. Will not be considered." << std::endl;}
      }
      
      continue;
    }
    if (sorting_order[0] == 0) {std::cout << "ERROR! In Twiss file there is no line starting with '*' which defines element type..." << std::endl; return;}
    
    if (in.peek() == 36) //36 is '$' symbol
    {
      //these lines MAD-X parameter types, can be skipped
      in.ignore(5000, '\n');
      continue;
    }
    
    if (in.peek() == EOF) break;
    
    if (verbose) for (int b=0; b<n_elements; b++) std::cout << sorting_order[b] << std::endl;
    
    getline(in, line);
    std::istringstream ss(line);
    unsorted_param.clear();
    for (std::string value; ss >> value; )
    {
      if (value.compare("IP1") != 0) IsIP1 = true;
      unsorted_param.push_back(value);
    }
    if (!IsIP1) continue; //some twiss files start before the IP... also position should be counted from IP1
    
    sorted_param.clear();
    for (int a=0; a<n_elements; a++) sorted_param.push_back(unsorted_param.at(sorting_order[a]));
    element.push_back(sorted_param);

	  //std::cout << i << sorted_param.at(14) << std::endl;
	  betax.push_back(stod(sorted_param.at(14)));
	  betay.push_back(stod(sorted_param.at(15)));
  }

  /*
  std::vector<double> temp;
  for (int i=0; i<betax.size(); i++)
  {
  	temp.push_back(sqrt((betax[i]*3.5e-6)/gamma_rel)*1000);
    temp.push_back(sqrt((betay[i]*3.5e-6)/gamma_rel)*1000);
  	beam_width.push_back(temp);
    temp.clear();
  }
  */

  //Determine Beampipe Center at Collimators
  double TCL4_old = GetTCL4Jaw();
  double TCL5_old = GetTCL5Jaw();
  double TCL6_old = GetTCL6Jaw();
  double TCL4_new = 999., TCL5_new = 999., TCL6_new = 999.;
  SetTCL4Jaw(999.);
  SetTCL5Jaw(999.);
  SetTCL6Jaw(999.);
  if (verbose) std::cout << "TCL4 position:" << std::endl;
  SimpleTracking(GetTCL4Pos(), 0., 0., 0., 0., 0., GetBeamEnergy(), false, false);
  SetTCL4Center(x);
  if (compute_sigma) TCL4_new = GetTCL4Sigma()*std::sqrt(GetEmittance()*0.938*sigma_x/GetBeamEnergy())*1.e-3;
  if (verbose) std::cout << "TCL5 position:" << std::endl;
  SimpleTracking(GetTCL5Pos(), 0., 0., 0., 0., 0., GetBeamEnergy(), false, false);
  SetTCL5Center(x);
  if (compute_sigma) TCL5_new = GetTCL5Sigma()*std::sqrt(GetEmittance()*0.938*sigma_x/GetBeamEnergy())*1.e-3;
  if (verbose) std::cout << "TCL6 position:" << std::endl;
  SimpleTracking(GetTCL6Pos(), 0., 0., 0., 0., 0., GetBeamEnergy(), false, false);
  SetTCL6Center(x);
  if (compute_sigma) TCL6_new = GetTCL6Sigma()*std::sqrt(GetEmittance()*0.938*sigma_x/GetBeamEnergy())*1.e-3;
  TCL4_old > 990. ? SetTCL4Jaw(TCL4_new) : SetTCL4Jaw(TCL4_old);
  TCL5_old > 990. ? SetTCL5Jaw(TCL5_new) : SetTCL5Jaw(TCL5_old);
  TCL6_old > 990. ? SetTCL6Jaw(TCL6_new) : SetTCL6Jaw(TCL6_old);
}

void ProtonTransport::SimpleTracking(double obs_point, double x0, double y0, double z0, double px0, double py0, double pz0, bool ShowEachStep=false, bool verbose=false){

  x = x0;
  y = y0;
  z = z0;
  px = px0 + cos(3.14159265*GetCrossingAnglePhi()/180.)*GetCrossingAngle()*GetBeamEnergy();
  py = py0 + sin(3.14159265*GetCrossingAnglePhi()/180.)*GetCrossingAngle()*GetBeamEnergy();
  pz = pz0;
  sx = px/pz;
  sy = py/pz;
  int flag = 0;

  BeampipesAreSeparated = false;
  proton_lost_pos = -1.;
  
  for (unsigned int a=0; a<element.size(); a++)
  {
    if (DoApertureCut) ProtonTransport::aperture_hit(a);
    if ((element[a].at(0)).compare("\"MARKER\"") == 0) ProtonTransport::Marker(verbose); //Marker(true) will return proton x, y, z, px, py, pz at position of marker
    else if ((element[a].at(0)).compare("\"DRIFT\"") == 0) ProtonTransport::simple_drift(stod(element[a].at(2)), verbose); // L
    else if ((element[a].at(0)).compare("\"RBEND\"") == 0) ProtonTransport::simple_rectangular_dipole(stod(element[a].at(2)), stod(element[a].at(5)), verbose); //L, K0L
    else if ((element[a].at(0)).compare("\"HKICKER\"") == 0) ProtonTransport::simple_horizontal_kicker(stod(element[a].at(2)), stod(element[a].at(3)), verbose); //L, HKICK
    else if ((element[a].at(0)).compare("\"VKICKER\"") == 0) ProtonTransport::simple_vertical_kicker(stod(element[a].at(2)), stod(element[a].at(4)), verbose); //L, VKICK
    else if ((element[a].at(0)).compare("\"QUADRUPOLE\"") == 0) ProtonTransport::simple_quadrupole(stod(element[a].at(2)), stod(element[a].at(6)), verbose); //L, K1L 
    else if ((element[a].at(0)).compare("\"MULTIPOLE\"") == 0)
    {
      if ( fabs(stod(element[a].at(5))) > 1.e-10 )
      {
        std::cout << "Warning! MULTIPOLE taken as rectangular dipole! Check in twiss files if this is correct! Position: " << element[a].at(1) << std::endl;
        ProtonTransport::simple_rectangular_dipole(stod(element[a].at(2)), stod(element[a].at(5)), verbose); //L, K0L
      }
      else if ( fabs(stod(element[a].at(3))) > 1.e-10 )
      {
        std::cout << "Warning! MULTIPOLE taken as horizontal kicker! Check in twiss files if this is correct! Position: " << element[a].at(1) << std::endl;
        ProtonTransport::simple_horizontal_kicker(stod(element[a].at(2)), stod(element[a].at(3)), verbose); //L, HKICK
      }
      else if ( fabs(stod(element[a].at(4))) > 1.e-10 )
      {
        std::cout << "Warning! MULTIPOLE taken as vertical kicker! Check in twiss files if this is correct! Position: " << element[a].at(1) << std::endl;
        ProtonTransport::simple_vertical_kicker(stod(element[a].at(2)), stod(element[a].at(4)), verbose); //L, VKICK
      }
      else if ( fabs(stod(element[a].at(6))) > 1.e-10 )
      {
        std::cout << "Warning! MULTIPOLE taken as quadrupole! Check in twiss files if this is correct! Position: " << element[a].at(1) << std::endl;
        ProtonTransport::simple_quadrupole(stod(element[a].at(2)), stod(element[a].at(6)), verbose); //L, K1L
      }
      else
      {
//TODO        std::cout << "Warning! MULTIPOLE taken as drift! Check in twiss files if this is correct! Position: " << element[a].at(1) << std::endl;
        ProtonTransport::simple_drift(stod(element[a].at(2)), verbose); // L
      }
    }
    else if (((element[a].at(0)).compare("\"COLLIMATOR\"") == 0) || ((element[a].at(0)).compare("\"RCOLLIMATOR\"") == 0)) ProtonTransport::simple_collimator(stod(element[a].at(2)), verbose);
/*
   The following elements are taken as a drift (if L!=0) or monitor (if L=0):
      * SOLENOID (TODO)
      * RCOLLIMATOR (TODO)
      * MONITOR (L=0 anyway)
      * PLACEHOLDER
      * INSTRUMENT
*/
    else
    {
      //std::cout << "Unidentified element:\t" << element[a].at(0) << std::endl;
      if (stod(element[a].at(2)) > 1.e-10) ProtonTransport::simple_drift(stod(element[a].at(2)), verbose);
      else ProtonTransport::Marker(verbose);
    }
    if (DoApertureCut) ProtonTransport::aperture_hit(a);
    
    if (ShowEachStep)
    {
      /*
      std::cout << "z: " << z;
      std::cout << "\tx: " << x;
      std::cout << "\ty: " << y;
      std::cout << "\tsx: " << sx;
      std::cout << "\tsy: " << sy;
      proton_lost_pos < 0. ? std::cout << "\t proton_lost? " << "not_lost" : std::cout << "\t proton_lost?:" << proton_lost_pos;
      std::cout << "\tbeta_x: " << element[a].at(14);
      std::cout << "\tbeta_y: " << element[a].at(15);
      std::cout << std::endl;
      */
      
      std::vector<double> temp1{sqrt((stod(element[a].at(14))*3.5e-6)/gamma_rel)*1000,sqrt((stod(element[a].at(15))*3.5e-6)/gamma_rel)*1000,z};
      beam_width.push_back(temp1);
      temp1.clear();
      
      std::vector<double> temp2{x,y,z};
      beam_trajectory.push_back(temp2);
      temp2.clear();
      //beam_trajectory[0].push_back(x);
      //beam_trajectory[1].push_back(y);
      //beam_trajectory[2].push_back(z);
    }

	  if (z > 127. && !BeampipesAreSeparated)
    {
      BeampipesAreSeparated = true;
      x += BeampipeSeparation;
    }

    if (z > obs_point)
    {
      if (flag==0)
      {
        flag=1;
        x = x - sx*(z - obs_point);
        y = y - sy*(z - obs_point);
        if (compute_sigma)
        {
          sigma_x = stod(element[a].at(14));
          sigma_y = stod(element[a].at(15));
        }
        if (ShowEachStep)
        {
          beam_center.push_back(stod(element[a].at(14)));
          beam_center.push_back(stod(element[a].at(15)));
          beam_center.push_back(proton_lost_pos);
          beam_center.push_back(x);
          beam_center.push_back(y);
        }
      }
      break;
    } 
  }
}

void ProtonTransport::NominalTrajectory(double energy=-1.){
  if (energy < 0.) energy = beam_energy;
  std::cout << "Nominal trajectory (collimators wide opened)." << std::endl;
  double TCL4_old = GetTCL4Jaw();
  double TCL5_old = GetTCL5Jaw();
  double TCL6_old = GetTCL6Jaw();
  SimpleTracking(250., 0., 0., 0., 0., 0., energy, true, false);
  SetTCL4Jaw(TCL4_old);
  SetTCL5Jaw(TCL5_old);
  SetTCL6Jaw(TCL6_old);
}


























